#include <sourcemod>
#include <sdktools>
#include <cstrike>
#include <multicolors>

ConVar g_CVarEnabled;
bool g_bEnabled;
ConVar g_CVarMoveToSpec;
ConVar g_CVarAFKVerdictTime;
ConVar g_CVarSpawnMoveTime;
ConVar g_CVarMinPlayersToEnable;
ConVar g_CVarKickPlayer;
ConVar g_CVarTimeToKick;
ConVar g_CVarMinPlayersToKick;
ConVar g_CVarWarnTimeToMove;
ConVar g_CVarWarnTimeToKick;
ConVar g_CVarExcludeDead;

Handle g_AFKFrameTimer;

Handle g_OnAFKForward;
Handle g_OnBackForward;

bool g_IsAFKed[MAXPLAYERS+1];
int g_AFKTime[MAXPLAYERS+1] = {0,...};
int g_PreviousButtons[MAXPLAYERS+1];
int g_PreviousMouse[MAXPLAYERS+1][2];
float g_LastRecvTime[MAXPLAYERS+1];

public Plugin myinfo =
{
	name = "AFK Manager 2",
	author = "X Community",
	description = "Takes action on AFK players",
	version = "1.1",
	url = ""
};

public APLRes AskPluginLoad2(Handle plugin, bool late, char[] error, int err_max)
{
	CreateNative("AFKM_IsClientAFK", Native_IsClientAFK);
	CreateNative("AFKM_GetClientAFKTime", Native_GetClientAFKTime);
	CreateNative("AFKM_GetClientAFKReallyTime", Native_GetClientAFKReallyTime);
	
	RegPluginLibrary("afkmanager");
	
	return APLRes_Success;
}

public void OnPluginStart()
{
	LoadTranslations("afk_manager2.phrases");
	
	g_OnAFKForward = CreateGlobalForward("AFKM_OnClientAFK", ET_Ignore, Param_Cell);
	g_OnBackForward = CreateGlobalForward("AFKM_OnClientBack", ET_Ignore, Param_Cell);
	
	g_CVarEnabled = CreateConVar("sm_afk_enable", "1", "Is the AFK Manager enabled or disabled? [0 = FALSE, 1 = TRUE, DEFAULT: 1]", FCVAR_NONE, true, 0.0, true, 1.0);
	g_bEnabled = g_CVarEnabled.BoolValue;
	g_CVarEnabled.AddChangeHook(ConVar_OnEnableChange);
	
	g_CVarMoveToSpec = CreateConVar("sm_afk_move_spec", "1", "Should the AFK Manager move AFK clients to spectator team? [0 = FALSE, 1 = TRUE, DEFAULT: 1]", FCVAR_NONE, true, 0.0, true, 1.0);
	g_CVarAFKVerdictTime = CreateConVar("sm_afk_move_time", "120.0", "Time in seconds (total) client must be AFK before being moved to spectator. [0 = DISABLED, DEFAULT: 120.0 seconds]");
	g_CVarSpawnMoveTime = CreateConVar("sm_afk_spawn_time", "0.0", "Time in seconds (total) that player should have moved from their spawn position. [0 = DISABLED, DEFAULT: 20.0 seconds]");
	g_CVarMinPlayersToEnable = CreateConVar("sm_afk_move_min_players", "4", "Minimum number of connected clients required for AFK move to be enabled. [DEFAULT: 4]");
	g_CVarKickPlayer = CreateConVar("sm_afk_kick_players", "0", "Should the AFK Manager kick AFK clients? [0 = DISABLED, 1 = KICK]");
	g_CVarTimeToKick = CreateConVar("sm_afk_kick_time", "300.0", "Time in seconds (total) client must be AFK before being kicked. [0 = DISABLED, DEFAULT: 300.0 seconds]");
	g_CVarMinPlayersToKick = CreateConVar("sm_afk_kick_min_players", "16", "Minimum number of connected clients required for AFK kick to be enabled. [DEFAULT: 6]");
	g_CVarWarnTimeToMove = CreateConVar("sm_afk_move_warn_time", "30.0", "Time in seconds remaining, player should be warned before being moved for AFK. [DEFAULT: 30.0 seconds]");
	g_CVarWarnTimeToKick = CreateConVar("sm_afk_kick_warn_time", "30.0", "Time in seconds remaining, player should be warned before being kicked for AFK. [DEFAULT: 30.0 seconds]");
	g_CVarExcludeDead = CreateConVar("sm_afk_exclude_dead", "1", "Should the AFK Manager exclude checking dead players? [0 = FALSE, 1 = TRUE, DEFAULT: 1]", FCVAR_NONE, true, 0.0, true, 1.0);
	
	g_AFKFrameTimer = CreateTimer(1.0, Timer_OnAFKFrame, _, TIMER_REPEAT);
	
	AddCommandListener(CommandListener_OnClientSay, "say");
	AddCommandListener(CommandListener_OnClientSay, "say_team");
	
	HookEvent("player_spawn", Event_PlayerSpawn);
	
	AutoExecConfig(true, "afk_manager");
}

public void OnPluginEnd()
{
	if(g_AFKFrameTimer != null)
		KillTimer(g_AFKFrameTimer);
}

public int Native_IsClientAFK(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	return g_IsAFKed[client];
}

public int Native_GetClientAFKTime(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	if (!g_IsAFKed[client]) {
		return 0;
	}
	return g_AFKTime[client];
}

public int Native_GetClientAFKReallyTime(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	return g_AFKTime[client];
}

public void ConVar_OnEnableChange(ConVar convar, const char[] oldValue, const char[] newValue)
{
	if (!convar.BoolValue) {
		for (int client = 1; client <= MAXPLAYERS; client++) {
			OnClientDisconnect(client);
		}
	}
	g_bEnabled = view_as<bool>(StringToInt(newValue));
}

public Action Event_PlayerSpawn(Handle event, const char[] name, bool dontBroadcast)
{
	int spawnMoveTime = g_CVarSpawnMoveTime.IntValue;
	if (spawnMoveTime <= 0) {
		return Plugin_Continue;
	}
	
	int players = 0;
	for (int client = 1; client <= MaxClients; client++) {
		if (IsClientInGame(client)) {
			players++;
		}
	}
	if (!g_CVarEnabled.BoolValue || g_CVarMinPlayersToEnable.IntValue > players) {
		return Plugin_Continue;
	}
	
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	int verdictTime = g_CVarAFKVerdictTime.IntValue;
	if (IsPlayerAlive(client) && !g_IsAFKed[client]) {
		g_AFKTime[client] =  verdictTime - spawnMoveTime;
	}
	
	return Plugin_Continue;
}

public void OnClientDisconnect(client)
{
	g_IsAFKed[client] = false;
	g_AFKTime[client] = 0;
	g_PreviousButtons[client] = 0;
	g_PreviousMouse[client][0] = 0;
	g_PreviousMouse[client][1] = 0;
	g_LastRecvTime[client] = 0.0;
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon, int &subtype, int &cmdnum, int &tickcount, int &seed, int mouse[2])
{
	if (g_bEnabled)
	{
		if (IsClientSourceTV(client) || IsFakeClient(client)) // Ignore Source TV & Bots
			return Plugin_Continue;
		
		if (cmdnum <= 0) // NULL Commands?
			return Plugin_Handled;
		
		if (g_PreviousButtons[client] != buttons || g_PreviousMouse[client][0] != mouse[0] || g_PreviousMouse[client][1] != mouse[1]) {
			g_PreviousButtons[client] = buttons;
			g_PreviousMouse[client][0] = mouse[0];
			g_PreviousMouse[client][1] = mouse[1];
			ResetAFK(client);
		}
		g_LastRecvTime[client] = GetEngineTime();
	}
	
	return Plugin_Continue;
}

public Action CommandListener_OnClientSay(int client, const char[] command, int argc)
{
	ResetAFK(client);
	g_LastRecvTime[client] = GetEngineTime();
	return Plugin_Continue;
}


public Action Timer_OnAFKFrame(Handle timer)
{
	int players = 0;
	for (int client = 1; client <= MaxClients; client++) {
		if (IsClientInGame(client) && !IsFakeClient(client) && !IsClientSourceTV(client)) {
			players++;
		}
	}
	if (!g_bEnabled || g_CVarMinPlayersToEnable.IntValue > players) {
		return Plugin_Continue;
	}
	
	int verdictTime = g_CVarAFKVerdictTime.IntValue;
	int kickTime = g_CVarTimeToKick.IntValue;
	int warnTimeMove = g_CVarWarnTimeToMove.IntValue;
	int warnTimeKick = g_CVarWarnTimeToKick.IntValue;
	float curEngineTime = GetEngineTime();
	
	for (int client = 1; client <= MaxClients; client++) {
		if (!IsClientInGame(client) || IsFakeClient(client) || IsClientSourceTV(client)) {
			continue;
		}
		if (g_CVarExcludeDead.BoolValue && !IsPlayerAlive(client) && GetClientTeam(client) > CS_TEAM_SPECTATOR) {
			continue;
		}
		// If we didn't receive the data more than 1 second, we don't process this client.
		if (curEngineTime - g_LastRecvTime[client] >= 1.0) {
			continue;
		}
		g_AFKTime[client] += 1;
		//PrintToServer("%N afk:%d",client,g_AFKTime[client]);
		if (!g_IsAFKed[client]) {
			if (verdictTime <= 0) {
				continue;
			}
			int verdictTimeLeft = verdictTime - g_AFKTime[client];
			if (verdictTimeLeft > 0 && verdictTimeLeft <= warnTimeMove && (verdictTimeLeft % 10) == 0) {
				if(warnTimeMove > 0){
					SetGlobalTransTarget(client);
					CPrintToChat(client, "{olive}[{green}AFK{olive}] {default}%t", "Flag_Warn_Message", verdictTimeLeft);
				}
			}
			else if (verdictTimeLeft <= 0) {
				OnClientAFKVerdicted(client);
				g_IsAFKed[client] = true;
				g_AFKTime[client] = 0;
			}
		}
		else if (g_CVarMinPlayersToKick.IntValue <= players) {
			if (kickTime <= 0) {
				continue;
			}
			int kickTimeLeft = kickTime - g_AFKTime[client];
			if (kickTimeLeft > 0 && kickTimeLeft <= warnTimeKick && ((kickTimeLeft) % 10) == 0) {
				SetGlobalTransTarget(client);
				CPrintToChat(client, "{olive}[{green}AFK{olive}] {default}%t", "Kick_Warn_Message", kickTimeLeft);
			}
			else if (kickTimeLeft <= 0 && g_CVarKickPlayer.BoolValue) {
				KickClient(client, "[AFK] %t", "Kick_Message");
				g_AFKTime[client] = 0;
			}
		}
	}
	
	return Plugin_Continue;
}

void OnClientAFKVerdicted(int client)
{
	if (g_CVarMoveToSpec.BoolValue) {
		int team = GetClientTeam(client);
		if (team == CS_TEAM_T || team == CS_TEAM_CT) {
			if (IsPlayerAlive(client)) {
				ForcePlayerSuicide(client);
			}
			ChangeClientTeam(client, CS_TEAM_SPECTATOR);
			
			SetGlobalTransTarget(client);
			CPrintToChat(client, "{olive}[{green}AFK{olive}] {default}%t", "Flag_Message");
		}
	}
	Call_StartForward(g_OnAFKForward);
	Call_PushCell(client);
	Call_Finish();
}

void ResetAFK(int client)
{
	if (g_IsAFKed[client]) {
		Call_StartForward(g_OnBackForward);
		Call_PushCell(client);
		Call_Finish();
	}
	g_IsAFKed[client] = false;
	g_AFKTime[client] = 0;
}

public void OnClientConnected(int client)
{
	g_IsAFKed[client] = false;
	g_AFKTime[client] = 0;
}
