#include <sourcemod>
StringMap StoreAdmins = null;

ConVar g_CvarEnable;

public Plugin myinfo = 
{
	name = "PreLoad VIP Admins",
	author = "X",
	description = "",
	version = "1.3",
	url = ""
}

int VIPCount = 0;
int LastID = 0;
int CurPreCount = 0;
Database g_hDb = null;

bool PreloadEnd = false;

File g_File = null;

int updateTime = 0;

public void OnPluginStart()
{
	StoreAdmins = new StringMap();
	g_CvarEnable = CreateConVar("sm_preload_adminvip_enable", "1", "enabled or disabled? PreLoad Store Adm&VIPUsers? [0 = FALSE, 1 = TRUE, DEFAULT: 1]", FCVAR_NONE, true, 0.0, true, 1.0);
	RegAdminCmd("sm_admin_preload_check", Command_preloadCheck, ADMFLAG_BAN);
	RegAdminCmd("sm_admin_preload_update", Command_preloadUpdate, ADMFLAG_BAN);
	RegAdminCmd("sm_admin_preload_recache", Command_preloadRecache, ADMFLAG_BAN);
	AutoExecConfig(true, "PreLoadAdminVIP");
	
	char path[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, path, sizeof(path), "data/preloadadminvip.bin");
	
	char path2[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, path2, sizeof(path2), "data/preloadadminvip_updatetime.ini");
	
	char timestamp[20];
	Handle fileHandle=OpenFile(path2,"r"); 
	
	if(fileHandle != null)
	{
		ReadFileLine(fileHandle, timestamp, 20);
		delete fileHandle;
	}
	
	if(GetTime()-259200 >= StringToInt(timestamp) || StringToInt(timestamp) <= 0)
	{
		ClearFile();
		fileHandle=OpenFile(path2,"w");
		WriteFileLine(fileHandle,"%d",GetTime());
		delete fileHandle;
	}
	
	
	InitFile();
	
	
	SQL_ConnectDB();
}



public Action Command_preloadRecache(int client, int args) 
{
	ClearFile();
}


void InitFile()
{
	char path[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, path, sizeof(path), "data/preloadadminvip.bin");
	
	g_File = OpenFile(path, "ab+");
	if (g_File != null) {
		int accountid, dbid;
		while (g_File.ReadInt32(accountid) && g_File.ReadInt32(dbid)) {
			if (LastID < dbid) {
				LastID = dbid;
			}
			PreLoadAdmin(accountid);
		}
	}
}

void ClearFile()
{
	if(g_File != null)
	{
		g_File.Close();
		g_File = null;
	}
	
	char path[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, path, sizeof(path), "data/preloadadminvip.bin");
	g_File = OpenFile(path, "w");
	
	if(g_File != null)
	{
		g_File.Close();
		g_File = null;
	}
	
	char path2[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, path2, sizeof(path2), "data/preloadadminvip_updatetime.ini");
	g_File = OpenFile(path2, "w");
	if(g_File != null)
	{
		g_File.Close();
		g_File = null;
	}
	
	
}



void SQL_ConnectDB()
{
	if(!SQL_CheckConfig("storetest"))
		return;
	
	Database.Connect(Callback_ConnectDB, "storetest");
}

public void Callback_ConnectDB(Database db, const char[] error, int data)
{
	if (db == null) 
	{
		LogMessage("Error");
		return;
	}
	
	g_hDb = db;
	
	CreateTimer(20.0,DBInit);
}


public Action DBInit(Handle timer)
{
	if (g_CvarEnable.BoolValue)
	{
		SQL_Read_VIPS(LastID);
	}
	return Plugin_Stop;
}

//UPDATE
public void OnConfigsExecuted()
{
	if(g_CvarEnable.BoolValue)
	{
		CheckStuck();
		if(PreloadEnd)
		{
			SQL_Read_VIPS(LastID);
			//LogMessage("Pre Load Adm&VIPS!");
		}
	}
	
}
//UPDATE




void SQL_Read_VIPS(int StartID)
{
	CurPreCount = 0;
	
	PreloadEnd = false;
	updateTime = GetTime();
	
	if(g_hDb != null)
	{
		char query[255];
		Format(query,255,"SELECT id,player_id FROM store_items where type = 'admin' AND id > '%d'",StartID);
		SQL_TQuery(g_hDb, T_BuildVIPS,query);
	}
}


public void T_BuildVIPS(Handle owner, Handle hndl, char[] error, any data)
{
	if (hndl == INVALID_HANDLE)
	{
		PreloadEnd = true;
		
		if(g_File != null)
		{
			//STOP Cache missing data
			ClearFile();
			delete(g_File);
			//STOP Cache missing data 
		}
		
		LogError("Query failed! %s", error);
		return;
	}
	VIPCount = SQL_GetRowCount(hndl);
	if (VIPCount > 0 && SQL_HasResultSet(hndl))
	{
		while(SQL_FetchRow(hndl))
		{
			int id = SQL_FetchInt(hndl,0);
			if (LastID < id) {
				LastID = id;
			}
			SQL_Get_VIP_Steam(SQL_FetchInt(hndl,1), id);
		}
	}else 
	{
		PreloadEnd = true;
		if(g_File != null)
			delete g_File;
	}
	PrintToServer("LastID = %d",LastID);
}

void SQL_Get_VIP_Steam(int PlayerID, int dbID)
{
	char query[255];
	Format(query,255,"SELECT authid FROM store_players where id = '%d'",PlayerID);
	SQL_TQuery(g_hDb, T_SQL_Get_VIP_Steam_CallBack,query, dbID);
}

public void T_SQL_Get_VIP_Steam_CallBack(Handle owner, Handle hndl, char[] error, int dbID)
{
	CurPreCount++;
	
	if(CurPreCount == VIPCount)
	{
		PreloadEnd = true;
		
		if(g_File != null)
		{
			delete g_File;
		}
	}
	
	if (hndl == INVALID_HANDLE)
	{
		if(g_File != null)
		{
			//STOP Cache missing data
			ClearFile();
			delete(g_File);
			//STOP Cache missing data 
		}
		
		LogError("Query failed! %s", error);
		return;
	}
	
	while(SQL_FetchRow(hndl))
	{
		if(SQL_GetRowCount(hndl) && SQL_HasResultSet(hndl))
		{
			char Authid[32];
			SQL_FetchString(hndl, 0, Authid, sizeof(Authid));
			Format(Authid,sizeof(Authid),"STEAM_1:%s",Authid);
			int accountid = GetSteamIDAccountID(Authid);
			if (accountid != 0) {
				StoreAccountIDToFileDB(accountid, dbID);
				PreLoadAdmin(accountid);
			} else {
				LogError("Cannot preload steamid: %s", Authid);
			}
		}
	}
	
	PrintToServer(" Adm&VIP PreLoad [%d/%d]",CurPreCount,VIPCount);
}



//native bool StoreAdmins_CheckAdm(const char[] steam);
//native bool StoreAdmins_AddAdm(const char[] steam);

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	CreateNative("StoreAdmins_CheckAdm", Native_StoreAdmins_CheckAdm);
	CreateNative("StoreAdmins_AddAdm", Native_StoreAdmins_AddAdm);
	
	RegPluginLibrary("PreLoadAdminVIP");
	
	return APLRes_Success;
}

public int Native_StoreAdmins_CheckAdm(Handle plugin, int numParams)
{
	char buffer[32]; 
	GetNativeString(1, buffer,sizeof(buffer));
	int accountid = GetSteamIDAccountID(buffer);
	if (accountid != 0) {
		return StoreAdmins_GetAdmin(accountid);
	}
	return 0;
}

public int Native_StoreAdmins_AddAdm(Handle plugin, int numParams)
{
	char buffer[32]; 
	GetNativeString(1, buffer,sizeof(buffer));
	int accountid = GetSteamIDAccountID(buffer);
	if (accountid != 0) {
		StoreAccountIDToFileDB(accountid);
		return PreLoadAdmin(accountid);
	}
	return false;
}



public Action Command_preloadCheck(int client, int args) 
{
	if(client == 0)
	{
		int accountid = GetSteamIDAccountID("STEAM_1:1:55075208");
		if(StoreAdmins_GetAdmin(accountid))
			ReplyToCommand(client, "Find");
		else ReplyToCommand(client, "Not Find");
	}
	else
	{
		char auth[32];
		if(GetClientAuthId(client,AuthId_Steam2,auth,32))
		{	
			int accountid = GetSteamIDAccountID(auth);
			if(StoreAdmins_GetAdmin(accountid))
				ReplyToCommand(client, "Find");
			else ReplyToCommand(client, "Not Find");
		}
	}
	
	return Plugin_Handled;
}

public Action Command_preloadUpdate(int client, int args) 
{
	int TrieSize = StoreAdmins.Size;
	if(PreloadEnd)
	{
		SQL_Read_VIPS(LastID);
		ReplyToCommand(client, "Update...LastID = %d Size = %d",LastID,TrieSize);
	}
	else {
		CheckStuck();
		ReplyToCommand(client, "Pending. [%d/%d] Size = %d..stuck check:%d",CurPreCount,VIPCount,TrieSize,updateTime-(GetTime()-600));
	}
	return Plugin_Handled;
}

bool PreLoadAdmin(int accountid)
{
	if (StoreAdmins == null)
	{
		return false;
	}
	char key[32];
	FormatEx(key, sizeof(key), "%x", accountid);
	
	return StoreAdmins.SetValue(key, true);
}

bool StoreAdmins_GetAdmin(int accountid)
{
	if (StoreAdmins == null)
	{
		return false;
	}
	char key[32];
	FormatEx(key, sizeof(key), "%x", accountid);
	
	bool value;
	return StoreAdmins.GetValue(key,value);
}

void StoreAccountIDToFileDB(int accountid, int dbid = -1)
{
	if (g_File != null) {
		g_File.WriteInt32(accountid);
		g_File.WriteInt32(dbid);
	}
}




stock int GetSteamIDAccountID(const char[] id)
{
	if (strlen(id) < 11) {
		return 0;
	}
	int result;
	result = StringToInt(id[10]) * 2;
	result += id[8] - '0';
	return result;
}


//
void CheckStuck()
{
	if(!PreloadEnd && updateTime != 0 && updateTime <= (GetTime()-600))
	{
		if(CurPreCount == VIPCount)
			PreloadEnd = true;
		
		if(CurPreCount <= 0 || VIPCount <= 0)
			PreloadEnd = true;
	}
}

