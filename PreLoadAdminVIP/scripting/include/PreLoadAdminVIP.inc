#if defined _preloadadminvip_included
 #endinput
#endif
#define _preloadadminvip_included

native bool StoreAdmins_CheckAdm(const char[] steam);

native bool StoreAdmins_AddAdm(const char[] steam);

public SharedPlugin __pl_preloadadminvip = 
{
	name = "PreLoadAdminVIP",
	file = "PreLoadAdminVIP.smx",
#if defined REQUIRE_PLUGIN
	required = 1,
#else
	required = 0,
#endif
};

#if !defined REQUIRE_PLUGIN
public void __pl_preloadadminvip_SetNTVOptional()
{
	MarkNativeAsOptional("StoreAdmins_CheckAdm");
	MarkNativeAsOptional("StoreAdmins_AddAdm");
}
#endif