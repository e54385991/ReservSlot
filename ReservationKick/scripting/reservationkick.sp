#include <sourcemod>
#include <cstrike>
#include <onconnectclient>
#include <afk_manager>
#include <PreLoadAdminVIP>
#pragma semicolon 1

public Plugin myinfo = 
{
	name = "Reservation kick",
	author = "X Community",
	description = "Kick if admin entering",
	version = "0.1",
	url = ""
};

ConVar g_CVarReservedSlots;

public void OnPluginStart()
{
	LoadTranslations("reservationkick.phrases");
}

public void OnAllPluginsLoaded()
{
	if (g_CVarReservedSlots == null) {
		g_CVarReservedSlots = FindConVar("sm_reserved_slots");
	}
}

public Action OnConnectClient(const char[] sName, char sPassword[128], const char[] sIp, const char[] sSteamID, char rejectReason[512])
{
	int players, criteria;
	players = 0;
	if (g_CVarReservedSlots != null) {
		criteria = GetMaxHumanPlayers() - g_CVarReservedSlots.IntValue;
	}
	else {
		criteria = GetMaxHumanPlayers();
	}
	for (int client = 1; client <= MaxClients; client++) {
		if (IsClientConnected(client)) {
			players++;
		}
	}
	if (players < criteria) {
		return Plugin_Continue;
	}
	
	int privileged = 0;
	AdminId id = FindAdminByIdentity("steam", sSteamID);
	char steamid2[20];
	GetSteamId2(sSteamID, steamid2, sizeof(steamid2));
	if (id != INVALID_ADMIN_ID || GetAdminFlag(id, Admin_Root, Access_Effective)) {
		privileged = 2;
	}
	else if (StoreAdmins_CheckAdm(steamid2)) {
		privileged = 1;
	}
	
	int afk, afk2, ltime, ltime2;
	afk = 0;
	afk2 = 0;
	ltime = 0;
	ltime2 = 0;
	
	if (players < 64) {
		if (privileged > 0) {
			return Plugin_Continue;
		}
		for (int client = 1; client <= MaxClients; client++) {
			if (IsClientInGame(client) && !IsFakeClient(client) && AFKM_IsClientAFK(client)) {
				int time = AFKM_GetClientAFKTime(client);
				if (time < 30) {
					continue;
				}
				AdminId cid = GetUserAdmin(client);
				if (cid != INVALID_ADMIN_ID && GetAdminFlag(cid, Admin_Generic, Access_Effective)) {
					continue;
				}
				if (time > ltime) {
					afk = client;
					ltime = time;
				}
			}
		}
	}
	else {
		for (int client = 1; client <= MaxClients; client++) {
			if (IsClientInGame(client) && !IsFakeClient(client) && AFKM_IsClientAFK(client)) {
				int time = AFKM_GetClientAFKTime(client);
				if (time < 30) {
					continue;
				}
				AdminId cid = GetUserAdmin(client);
				if (cid != INVALID_ADMIN_ID) {
					if (GetAdminFlag(cid, Admin_Root, Access_Effective)) {
						continue;
					}
					else if (GetAdminFlag(cid, Admin_Generic, Access_Effective)) {
						if (time > ltime2) {
							afk2 = client;
							ltime2 = time;
						}
						continue;
					}
				}
				if (time > ltime) {
					afk = client;
					ltime = time;
				}
			}
		}
	}
	if (afk == 0 && afk2 != 0) {
		if (privileged >= 1) {
			afk = afk2;
		}
	}
	if (afk != 0) {
		KickClientEx(afk, "[AFK] %t", "Kick_Message");
		return Plugin_Continue;
	}
	
	if(players < 64)
	{
		Format(rejectReason, sizeof(rejectReason), "服务器已满人数设定:%d 如果您为VIP 仍可connect 进入.\n bbs.93x.net",criteria);
	}else strcopy(rejectReason, sizeof(rejectReason), "#Valve_Reject_Server_Full");
	
	return Plugin_Handled;
}

stock void GetSteamId2(const char[] szSteam64, char[] szSteam2, int iLen) 
{ 
	char szBase[18] = "76561197960265728"; 
	int iBorrow = 0; 
	char szSteam[18]; 
	char szAccount[18]; 
	int iY = 0; 
	int iZ = 0; 
	int iTemp = 0; 
	
	//Copy steamid64 Over 
	Format(szSteam, sizeof(szSteam), szSteam64); 
	
	//Determine iY 
	if (intval(szSteam[16]) % 2 == 1) { 
		iY = 1; 
		szSteam[16] = strval(intval(szSteam[16]) - 1); 
	} 
	
	//Determine szAccount 
	for (int k = 16; k >= 0; --k) { 
		if (iBorrow > 0) { 
			iTemp = intval(szSteam[k]) - 1; 
			
			if (iTemp >= intval(szBase[k])) { 
				iBorrow = 0; 
				szAccount[k] = strval(iTemp - intval(szBase[k])); 
			} 
			else { 
				iBorrow = 1; 
				szAccount[k] = strval((iTemp + 10) - intval(szBase[k])); 
			} 
		} 
		else { 
			if (intval(szSteam[k]) >= intval(szBase[k])) { 
				iBorrow = 0; 
				szAccount[k] = strval(intval(szSteam[k]) - intval(szBase[k])); 
			} 
			else { 
				iBorrow = 1; 
				szAccount[k] = strval((intval(szSteam[k]) + 10) - intval(szBase[k])); 
			}
		} 
	}
	
	//Divide szAccount answer by 2 
	iZ = StringToInt(szAccount) / 2; 
	
	//Construct final steam2 id 
	Format(szSteam2, iLen, "STEAM_1:%d:%d", iY, iZ); 
} 

int strval(const int iNum) 
{ 
	return '0' + ((iNum >= 0 && iNum <= 9) ? iNum : 0);
} 

int intval(int cNum) 
{ 
	return (cNum >= '0' && cNum <= '9') ? (cNum - '0') : 0;
}
