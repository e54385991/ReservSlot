/**
 * vim: set ts=4 :
 * =============================================================================
 * SourceMod Sample Extension
 * Copyright (C) 2004-2008 AlliedModders LLC.  All rights reserved.
 * =============================================================================
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 3.0, as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * As a special exception, AlliedModders LLC gives you permission to link the
 * code of this program (as well as its derivative works) to "Half-Life 2," the
 * "Source Engine," the "SourcePawn JIT," and any Game MODs that run on software
 * by the Valve Corporation.  You must obey the GNU General Public License in
 * all respects for all other code used.  Additionally, AlliedModders LLC grants
 * this exception to all derivative works.  AlliedModders LLC defines further
 * exceptions, found in LICENSE.txt (as of this writing, version JULY-31-2007),
 * or <http://www.sourcemod.net/license.php>.
 *
 * Version: $Id$
 */

#include "extension.h"
#include "netadr.h"
#include <inttypes.h>

#include "inetmessage.h"
#include "netmessages.pb.h"

template <int Type, class NetMessage, int Group, bool reliable>
class CNetMessagePB : public INetMessage, public NetMessage {
public:
	~CNetMessagePB() {}

};

typedef CNetMessagePB<16, CCLCMsg_SplitPlayerConnect, 0, true>	NetMsg_SplitPlayerConnect;

Extension g_Ext;		/**< Global singleton for extension's main interface */

SMEXT_LINK(&g_Ext);

IGameConfig *g_Config = nullptr;
ISDKTools *sdktools = nullptr;

IForward *g_Forward = nullptr;
IServer *iserver = nullptr;

SH_DECL_MANUALHOOK13(ConnectClient, 0, 0, 0, IClient *, const netadr_t &, int, int, int, const char *, const char *, const char *, int, CUtlVector<NetMsg_SplitPlayerConnect *> &, bool, CrossPlayPlatform_t, const unsigned char *, int);
SH_DECL_MANUALHOOK1_void_vafmt(RejectConnection, 0, 0, 0, const netadr_t &);

static IClient *OnConnectClient(const netadr_t & address, int nProtocol, int iChallenge, int nAuthProtocol, const char *pchName, const char *pchPassword, const char *pCookie, int cbCookie, CUtlVector<NetMsg_SplitPlayerConnect *> &pSplitPlayerConnectVector, bool bUnknown, CrossPlayPlatform_t platform, const unsigned char *pUnknown, int iUnknown);

bool Extension::SDK_OnLoad(char * error, size_t maxlength, bool late)
{
	char conferr[256];
	if (!gameconfs->LoadGameConfigFile("onconnectclient.games", &g_Config, conferr, sizeof(conferr))) {
		snprintf(error, maxlength, "Could not read onconnectclient.games: %s", conferr);
		return false;
	}

	sharesys->AddDependency(myself, "sdktools.ext", true, true);

	sharesys->RegisterLibrary(myself, "OnConnectClient");
	g_Forward = forwards->CreateForward("OnConnectClient", ET_Hook, 5, NULL, Param_String, Param_String, Param_String, Param_String, Param_String);

	return true;
}

void Extension::SDK_OnUnload()
{
	SH_REMOVE_MANUALHOOK(ConnectClient, iserver, SH_STATIC(OnConnectClient), false);

	forwards->ReleaseForward(g_Forward);
	gameconfs->CloseGameConfigFile(g_Config);
}

void Extension::SDK_OnAllLoaded()
{
	int offset;
	if (!g_Config->GetOffset("ConnectClient", &offset)) {
		smutils->LogError(myself, "Failed to get ConnectClient offset");
		return;
	}
	SH_MANUALHOOK_RECONFIGURE(ConnectClient, offset, 0, 0);
	if (!g_Config->GetOffset("RejectConnection", &offset)) {
		smutils->LogError(myself, "Failed to get RejectConnection offset");
		return;
	}
	SH_MANUALHOOK_RECONFIGURE(RejectConnection, offset, 0, 0);

	SM_GET_LATE_IFACE(SDKTOOLS, sdktools);
	iserver = sdktools->GetIServer();

	SH_ADD_MANUALHOOK(ConnectClient, iserver, SH_STATIC(OnConnectClient), false);
}

static const char *ExtractPlayerName(CUtlVector<NetMsg_SplitPlayerConnect *> &pSplitPlayerConnectVector)
{
	for (int i = 0; i < pSplitPlayerConnectVector.Count(); i++)
	{
		NetMsg_SplitPlayerConnect *split = pSplitPlayerConnectVector[i];
		if (!split->has_convars())
			continue;

		const CMsg_CVars cvars = split->convars();
		for (int c = 0; c < cvars.cvars_size(); c++)
		{
			const CMsg_CVars_CVar cvar = cvars.cvars(c);
			if (!cvar.has_name() || !cvar.has_value())
				continue;

			if (!strcmp(cvar.name().c_str(), "name"))
			{
				return cvar.value().c_str();
			}
		}
	}
	return "";
}

IClient *OnConnectClient(const netadr_t & address, int nProtocol, int iChallenge, int nAuthProtocol, const char *pchName, const char *pchPassword, const char *pCookie, int cbCookie, CUtlVector<NetMsg_SplitPlayerConnect *> &pSplitPlayerConnectVector, bool bUnknown, CrossPlayPlatform_t platform, const unsigned char *pUnknown, int iUnknown)
{
	if (nAuthProtocol == 3 && g_Forward->GetFunctionCount() > 0) {
		cell_t res = PLUGIN_CONTINUE;

		char rejectReason[512];
		char szSteamID[32];
		char passwordBuffer[128];
		char ipString[16];

		strncpy(passwordBuffer, pchPassword, sizeof(passwordBuffer));
		snprintf(ipString, sizeof(ipString), "%u.%u.%u.%u", address.ip[0], address.ip[1], address.ip[2], address.ip[3]);
		uint64 ullSteamID = *(uint64 *)pCookie;
		CSteamID SteamID = CSteamID(ullSteamID);
		snprintf(szSteamID, sizeof(szSteamID), "%" PRIu64, SteamID.ConvertToUint64());

		g_Forward->PushString(ExtractPlayerName(pSplitPlayerConnectVector));
		g_Forward->PushStringEx(passwordBuffer, sizeof(passwordBuffer), SM_PARAM_STRING_UTF8 | SM_PARAM_STRING_COPY, SM_PARAM_COPYBACK);
		g_Forward->PushString(ipString);
		g_Forward->PushString(szSteamID);
		g_Forward->PushStringEx(rejectReason, sizeof(rejectReason), SM_PARAM_STRING_UTF8 | SM_PARAM_STRING_COPY, SM_PARAM_COPYBACK);
		g_Forward->Execute(&res);

		if (res != Pl_Continue)
		{
			if (res == Pl_Changed)
			{
				//Not work:( RETURN_META_VALUE_MNEWPARAMS(MRES_HANDLED, nullptr, OnClientConnect, (address, nProtocol, iChallenge, nAuthProtocol, pchName, ((const char *)passwordBuffer), pCookie, cbCookie, pSplitPlayerConnectVector, bUnknown, platform, pUnknown, iUnknown));
				RETURN_META_VALUE(MRES_SUPERCEDE, SH_MCALL(iserver, ConnectClient)(address, nProtocol, iChallenge, nAuthProtocol, pchName, passwordBuffer, pCookie, cbCookie, pSplitPlayerConnectVector, bUnknown, platform, pUnknown, iUnknown));
			}
			else
			{
				SH_MCALL(iserver, RejectConnection)(address, rejectReason);
				RETURN_META_VALUE(MRES_SUPERCEDE, nullptr);
			}
		}
	}
	RETURN_META_VALUE(MRES_IGNORED, nullptr);
}