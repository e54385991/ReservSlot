#if defined _onconnectclient_included
 #endinput
#endif
#define _onconnectclient_included

forward Action OnConnectClient(const char[] sName, char sPassword[128], const char[] sIp, const char[] sSteamID, char rejectReason[512]);

public Extension __ext_onconnectclient = 
{
	name = "OnConnectClient",
	file = "onconnectclient.ext",
#if defined AUTOLOAD_EXTENSIONS
	autoload = 1,
#else
	autoload = 0,
#endif
#if defined REQUIRE_EXTENSIONS
	required = 1,
#else
	required = 0,
#endif
};